import React, { Component } from 'react';
import Layout from "./components/Layout/Layout";
import Page from "./containers/Page/Page";
import {BrowserRouter, Route} from "react-router-dom";
import EditPage from "./containers/EditPage/EditPage";
import Home from "./components/Home/Home";


class App extends Component {
  render() {
    return (
        <BrowserRouter>
          <Layout>
             <Route path="/" exact  component={Home}/>
             <Route path="/pages/:name" exact  component={Page}/>
              <Route path="/pages/admin/edit" exact component={EditPage}/>
          </Layout>
        </BrowserRouter>
    );
  }
}

export default App;
