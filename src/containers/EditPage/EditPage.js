import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import Container from "reactstrap/es/Container";
import axios from '../../axios-page';

class EditPage extends Component {
    state = {
        title: '',
        content: '',
        page: 'about'
    };

    valueChange = event => {
      const name = event.target.name ;
        this.setState({[name]: event.target.value })
    };

    componentDidMount() {
        axios.get(`${this.state.page}.json`).then(response => {
            this.setState({
                title: response.data.title,
                content: response.data.content
            })
        })
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.state.page !== prevState.page) {
            axios.get(`${this.state.page}.json`).then(response => {
                this.setState({
                    title: response.data.title,
                    content: response.data.content
                })
            })
        }
    }


    editPage = (event) => {
        event.preventDefault();
        const data = {
            title: this.state.title,
            content: this.state.content
        };
        axios.put(`${this.state.page}.json`, data).then( () => {
            this.props.history.push(`/pages/${this.state.page}`)
        })
    };



    render() {
        return (
            <Container >
            <Form className="ProductForm mt-5"  mrt={10} onSubmit={(event) => this.editPage(event)}>
                <h1>Edit pages</h1>
                <FormGroup row>
                    <Label for="exampleEmail" sm={2}>Select page</Label>
                    <Col sm={2}>
                        <Input type="select"  name="page" id="cotegory" value={this.state.page} onChange={this.valueChange}
                        >
                            <option>about</option>
                            <option>contacts</option>
                            <option>divisions</option>
                        </Input>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="exampleEmail" sm={2}>Title</Label>
                    <Col sm={10}>
                        <Input type="text" name="title" id="name"
                               value={this.state.title}
                               onChange={this.valueChange}
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="exampleEmail" sm={2}>Content</Label>
                    <Col sm={10}>
                        <Input type="textarea" name="content"
                               value={this.state.content}
                               onChange={this.valueChange}
                        />
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Col sm={{size: 10, offset: 2}}>
                        <Button type="submit"  color="primary">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
            </Container>
        );
    }
}

export default EditPage;