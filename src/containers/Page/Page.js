import React, {Component} from 'react';

import axios from '../../axios-page'
import {Container, Jumbotron} from "reactstrap";

class Page extends Component {
    state = {
      pages: null
    };


    loadData = () => {
        const name = this.props.match.params.name;
        axios.get(name + ".json").then(response => {
            this.setState({pages: response.data})
        })
    };
    componentDidMount() {
        this.loadData()
        console.log(this.props.match.params);
    }

    componentDidUpdate(prevProps) {
        console.log(this.props.match.params.name);
        if(this.props.match.params.name && this.props.match.params.name !== prevProps.match.params.name) {
            this.loadData()
        }
    }

    render() {
        if(!this.state.pages) return null;
        return (
            <Container className="mt-5">
                <Jumbotron>
                    <h1 className="display-3">{this.state.pages.title}</h1>
                    <p className="lead">{this.state.pages.content}</p>
                </Jumbotron>
            </Container>
        )
    }
}

export default Page;