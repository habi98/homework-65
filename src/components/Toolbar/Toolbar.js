import React from 'react';
import { Container,  Navbar,} from "reactstrap";
import NavigationItem from "../NavigationItem/NavigationItem";
import Logo from "../Logo/Logo";


const Toolbar = () => (
    <Navbar  className="navbar navbar-dark bg-primary"  expand="md">
        <Container>
           <Logo/>
          <NavigationItem/>
        </Container>
    </Navbar>
);

export default Toolbar;