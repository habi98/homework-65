import React from 'react';
import { Jumbotron} from "reactstrap";

const Home = () => {
    return (
        <Jumbotron>
            <h1 className="display-3">Home</h1>
            <p className="lead">This is home page</p>
        </Jumbotron>
    );
};

export default Home;