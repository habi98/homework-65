import React, {Fragment} from 'react';
import Toolbar from "../Toolbar/Toolbar";

const Layout = ({children}) => {
    return (
        <Fragment>
          <Toolbar/>
            <div className="Layout">
                {children}
            </div>
        </Fragment>
    );
};

export default Layout;