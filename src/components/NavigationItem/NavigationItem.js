import React, {Component} from 'react';
import {Collapse, Nav, NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

class NavigationItem extends Component {
    componentDidMount() {
        console.log(this.props);
    }

    render() {
        return (
            <Collapse  navbar>
                <Nav className="ml-auto" navbar>
                    <NavItem>
                        <NavLink tag={RouterNavLink} to="/">Home</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink tag={RouterNavLink} to="/pages/about">About</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink tag={RouterNavLink} to="/pages/contacts">Contacts</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink tag={RouterNavLink} to="/pages/divisions">Divisons</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink tag={RouterNavLink} to="/pages/admin/edit">Admin</NavLink>
                    </NavItem>
                </Nav>
            </Collapse>
        );
    }
}

export default NavigationItem;